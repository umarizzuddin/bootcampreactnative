function readBooksPromise (time,book,spent) {
    console.log(`saya mulai membaca buku ${book}`)
    return new promise ( function (resolve, reject){
        setTimeout(function(){
            let sisaWaktu = time - spent
            if(sisaWaktu >= 0){
                console.log(`saya sudah selesai membaca ${book}, sisa waktu saya ${sisaWaktu}`)
                resolve(sisaWaktu)
            } else {
                console.log(`saya sudah tidak punya waktu untuk baca ${book}`)
                reject(sisaWaktu)
            }
        }, spent)
    })
}

module.exports = readBooksPromise