var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
var time = 10000
for(var aa = 0 ; aa < books.length ; aa += 1){
    var book = {
        timeSpent : books[aa].timeSpent,
        name : books[aa].name
    }
    console.log(readBooksPromise(time, book.name, book.timeSpent));
} 

function readboook() {
    readBooksPromise
        .then(function (fulfilled) {
            // yay, you got a new phone
            console.log(fulfilled);
         // output: { brand: 'Samsung', color: 'black' }
        })
        .catch(function (error) {
            // oops, mom don't buy it
            console.log(error.message);
         // output: 'mom is not happy'
        });
    }
}

readboook()
// Lanjutkan code untuk menjalankan function readBooksPromise 