function readBooks(time, book, timeSpent, callback) {
    //console.log(time, book, callback)//cuman buat tester step nilai saja
    setTimeout(function() {
        let sisaWaktu = 0
        if(time > timeSpent) {
            sisaWaktu = time - timeSpent
            console.log(`saya membaca ${book}`)
            console.log(`saya sudah membaca ${book}, sisa waktu saya adalah ${sisaWaktu}`)
            callback(sisaWaktu) //menjalankan function callback
        }
        else {
            console.log(`saya membaca ${book}`)
            console.log('waktu saya habis')
            callback(time)
        }
    }, timeSpent)
}
module.exports = readBooks
