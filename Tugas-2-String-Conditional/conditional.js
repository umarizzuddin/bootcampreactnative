console.log('.');
console.log('bismillahirrahmanirrahim');
console.log('jawaban Umar -- tugas hari ke 2 -- conditional.js :');
console.log('\nBAGIAN SATU -- IF ELSE');

const readline = require("readline");
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

console.log('\nloading...');
console.log('\n.');
console.log(`\nselamat datang dalam game, silahkan isi nama dan pilih peran untuk memulai game`);
console.log('dalam dunia werewolf, ada 3 peran yang bisa kamu pilih :');
console.log('a. PENYIHIR      b. GUARDIAN     c. WEREWOLF');
rl.question("\nmasukkan nama kamu : ", function(name) {
    rl.question("peran yang kamu pilih (a/b/c): ", function(_work) {
        var nama = name
        var peran = _work
        console.log('.');
        if ( nama == "" ) {
            console.log("isi nama terlebih dahulu!");
        } else if ( peran == "a" ) {
            console.log("Selamat datang di Dunia Werewolf, " + nama );
            console.log("Halo Penyihir " + nama + ". sebagai penyihir yang hebat, kamu dapat melihat siapa yang menjadi werewolf!");
        } else if ( peran == "b" ) {
            console.log("Selamat datang di Dunia Werewolf, " + nama );
            console.log("Halo Guardian " + nama + ". sebagai guardian yang kuat, kamu akan membantu melindungi temanmu dari serangan werewolf.");
        } else if ( peran == "c" ) {
            console.log("Selamat datang di Dunia Werewolf, " + nama );
            console.log("Halo Werewolf " + nama + ". sebagai werewolf yang lapar, Kamu akan memakan mangsa setiap malam!");
        } else if ( peran == "" ) {
            console.log("Halo " + nama + ", kamu belum memilih peranan.");
            console.log('pilih peranan kamu untuk memulai permainan');
        } else {
            console.log("Maaf " + nama + ", Peran yang kamu pilih tidak tersedia");
        }
        console.log('.');
        console.log('mohon maaf, ' + nama + '... karena koneksi anda buruk, anda gagal terhubung ke server');
        console.log('\nGAME OVER   :))');
        rl.close();
    });
});

rl.on("close", function() {


    console.log('\n.');
    console.log('\nTUGAS BAGIAN 2 -- SWITH CASE');
    console.log('\nvar tanggal = 12');
    console.log('var bulan = 12');
    console.log('var tahun = 2012');
    console.log('\nswitch case =')
    var tanggal = 12; 
    var bulan = 12;
    var tahun = 2012;
    switch(bulan) {
        case 1 : { console.log(tanggal + ' januari ' + tahun); break; }
        case 2 : { console.log(tanggal + ' februari ' + tahun); break; }
        case 3 : { console.log(tanggal + ' maret ' + tahun); break; }
        case 4 : { console.log(tanggal + ' april ' + tahun); break; }
        case 5 : { console.log(tanggal + ' mei ' + tahun); break; }
        case 6 : { console.log(tanggal + ' juni ' + tahun); break; }
        case 7 : { console.log(tanggal + ' juli ' + tahun); break; }
        case 8 : { console.log(tanggal + ' agustus ' + tahun); break; }
        case 9 : { console.log(tanggal + ' september ' + tahun); break; }
        case 10 : { console.log(tanggal + ' oktober ' + tahun); break; }
        case 11 : { console.log(tanggal + ' november ' + tahun); break; }
        case 12 : { console.log(tanggal + ' desember ' + tahun); break; }
    }    
    console.log("\nyesss, finally yang ini kelar juga :))");
    console.log('.');
    console.log('.');
    process.exit(0);
});


