console.log('.');
console.log('bismillahirrahmanirrahim');
console.log('jawaban Umar -- tugas hari ke 3 -- looping.js :');
console.log('.');

console.log('\nSOAL PERTAMA :');
console.log('.');
console.log('looping pertama :');
var flag1 = 2
while(flag1 < 21) {
    console.log(flag1 + ' - saya suka coding');
    flag1 += 2;
}
console.log('.');
console.log('looping kedua :');
var flag2 = 20
while(flag2 > 1) {
    console.log(flag2 + ' - saya akan menjadi seorang mobile developer');
    flag2 -= 2;
}

console.log('\n.');
console.log('SOAL KEDUA');
var flag3 = 1
for(flag3 = 1; flag3 < 21; flag3++) {
    if(flag3 %2 != 0) {
        if(flag3 %3 == 0) {
        console.log(flag3 + ' - saya suka coding');
        } else {
            console.log(flag3 + ' - santai');
        }
    } else if(flag3 %2 == 0) {
        console.log(flag3 + ' - berkualitas');
    }

}

console.log('\nSOAL KETIGA');
for(var flag4 = 4; flag4 > 0; flag4--) {
    console.log('########');
}



console.log('\nSOAL KEEMPAT');
var flag5 = '';
for(var flag6 = 1; flag6 < 8; flag6++) {
    flag5 = flag5 + flag5 + '#';
    console.log(flag5);  
}



console.log('\nSOAL KELIMA');
for(var flag7 = 4; flag7 > 0; flag7--) {
    for(var flag8 = 1; flag8 > 0; flag8--) {
        console.log('# # # # ');    
    }
    console.log(' # # # #');    
}


console.log('\n.');
console.log('finally... selesai');
console.log('tugas day 3 -- Umar Izzuddin');
console.log('.');
