console.log('\n:::::::::::::::::::::---Bismillahirrahmanirrahim---:::::::::::::::::::::')
console.log(':::::::::::::::---tugas day 6 -- Umar Izzuddin -- object.js---::::::::::::::::')

console.log('\n---------------------SOAL PERTAMA---------------------')
function arraytoobject(arr){    
    for(var zz = 0; zz < arr.length; zz++){
        var now = new Date
        var today = now.getFullYear()
        var dt = {
            a : 'nama depan : ' + arr[zz][0] + ', ',
            b : 'nama belakang : ' + arr[zz][1] + ', ',
            c : 'gender : ' + arr[zz][2] + ', ',
            d : today-arr[zz][3]
        }
        if(dt.d<1)         {var e='data usia tidak valid}'}
        if(!dt.d)          {var e='data usia tidak valid}'}
        else if(dt.d>=1)   {var e='usia : '+dt.d+' tahun}'}
                        var awal = zz+1 +'. '+ arr[zz][0] +' '+ arr[zz][1] +' = {'
        console.log(awal + dt.a + dt.b + dt.c + e)        
    }    return ''
}
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
console.log('var people 1 :')
console.log(arraytoobject(people))
console.log('var people 2 :')
console.log(arraytoobject(people2))
console.log('untuk nomor 1 ini saya kurang paham harus dibuat output datar')
console.log('seperti di contoh, atau output menurun seperti di "code",')
console.log('jadi saya membuatnya datar seperti di contoh')

console.log('\n----------------------SOAL KEDUA----------------------')
function shoppingTime(memberId, money) {
    if(!memberId){console.log('Mohon maaf, toko X hanya berlaku untuk member saja')}
    else if(money<50000){console.log('Mohon maaf, uang anda tidak mencukupi')}
    else {
        console.log('Member ID : ' + memberId + ',\nsaldo : Rp' + money)
        var SALE = [['Sepatu Stacattu',1500000],['Baju Zoro',500000],['Baju H&N',250000],['Sweater Uniklooh',175000],['Casing Handphone',50000]]
        var urut = 0
        while(money >= 50000, urut < 5){
            //console.log(SALE[urut][1])
            if(money >= SALE[urut][1]){
                console.log(SALE[urut][0] + '-------Rp'+SALE[urut][1]);
                money -= SALE[urut][1]
                urut += 1
            } 
            else {urut += 1}
        }
        console.log('Sisa Saldo = ' + money)
    }  
    return '---------------------------------------------------';
}
   
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log('\n---------------------SOAL KETIGA---------------------')
console.log('----------------------------------------')
function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    if(!arrPenumpang[0]){console.log('data tidak valid')}
    else {
        for(var f = 0 ; f < arrPenumpang.length ; f++){
            var nama = 'Nama Penumpang : ' + arrPenumpang[f][0]
            var naik = ' , Naik Dari :' + arrPenumpang[f][1]
            var turun = ' , Destinasi :' + arrPenumpang[f][2]
            for(var test1 = 0 ; test1 < rute.length ; test1 +=1){
                if(arrPenumpang[f][1] !== rute[test1]){}
                else if(arrPenumpang[f][1] === rute[test1]){test1 += rute.length}
            }   var start = test1-rute.length
            for(var test2 = 0 ; test2 < rute.length ; test2 +=1){
                if(arrPenumpang[f][2] !== rute[test2]){}
                else if(arrPenumpang[f][2] === rute[test2]){test2 += rute.length}
            }   var end = test2-rute.length
            if(start>end){var bayar = (start - end)*2000} 
            else if(start<end){var bayar = (end - start)*2000} 
            else {var bayar = '---data tidak valid'}
            console.log(nama + naik + turun + ', bayar : Rp.'+bayar)
        }
    }
    
    return '----------------------------------'
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]


console.log('\n\n:::::::::::::::::::::---Selesai---:::::::::::::::::::::')
console.log('\n.')