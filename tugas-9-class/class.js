console.log('\n:::::::::::::::::::-----------bismillahirrahmanirrahim-----------:::::::::::::::::::')
console.log('::::::::::::::::::-----------tugas day 9 -- Umar -- Class---------::::::::::::::::::\n') 
console.log('SOAL PERTAMA :')
class Animal {
    constructor(species) {
        this.anname = species;
    }
    get name() {
        return this.anname;
    }
    set name(x) {
        this.anname = x;
    }
    get legs() {
        return '4'
    }
    get cold_blooded() {
        return(false)
    }
    // Code class di sini
}

var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
console.log('--------------------')

// Code class Ape dan class Frog di sini
class Ape {
    constructor(hewan) {
      this.suara = hewan;
    }
    yell(){console.log('Auooo')}
}

class Frog {
    constructor(hewan) {
      this.suara = hewan;
    }
    jump(){console.log('hop hop')}
}
 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

console.log('\n--------------------------------SOAL KEDUA--------------------------------')
class Clock {
    constructor({ template }) {
      this.template = template;
    }
  
    render() {
        var date = new Date();
  
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
  
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
  
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
  
        var output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);
  
        console.log(output);
    }
  
    stop() {
      clearInterval(this.timer);
    };
  
    start() {
      this.render();
      this.timer = setInterval(() => this.render(), 1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 




