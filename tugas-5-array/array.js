console.log('.')
console.log('bismillahirrahmanirrahim')
console.log('tugas hari ke 4 -- Umar Izzuddin -- array.js')
console.log('.')


console.log('\nSOAL PERTAMA')
function range(awal,akhir) { 
    var hasilnya = [];  
    if(!akhir){hasilnya.push(-1)}
    else if(awal<=akhir) {
        while(awal<=akhir){
            hasilnya.push(awal);
            awal++;
        }
    }
    else if(awal>=akhir) {
        while(awal>=akhir){
            hasilnya.push(awal)
            awal--;
        }
    }    

    return hasilnya;
}

console.log('console.log(range(1, 10))')
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log('console.log(range(1))')
console.log(range(1)) // -1
console.log('console.log(range(11,18))')
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log('console.log(range(54, 50))')
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log('console.log(range())')
console.log(range()) // -1



console.log('\n\nSOAL KEDUA')
function rangeWithStep(awal2, akhir2, step2) {
    var hasil2 = [];
    if(awal2<=akhir2) {
        while(awal2<=akhir2){
            hasil2.push(awal2);
            awal2+=step2;
        }
    }
    else if(awal2>=akhir2) {
        while(awal2>=akhir2){
            hasil2.push(awal2)
            awal2-=step2;
        }
    }    

    return hasil2;
}
console.log('console.log(rangeWithStep(1, 10, 2))')
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log('console.log(rangeWithStep(11, 23, 3))')
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log('console.log(rangeWithStep(5, 2, 1))')
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log('console.log(rangeWithStep(29, 2, 4))')
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 


console.log('\n\nSOAL KETIGA')
function sum(awal3,akhir3,step3){
    var hasil3 = [];
    if(!awal3){hasil3.push(0)}
    else if(!akhir3){hasil3.push(awal3)}
    else if(awal3<=akhir3) { 
        if(!step3){step3=1} 
        while(awal3<=akhir3){
            hasil3.push(awal3);
            awal3+=step3;
        }
    }
    else if(awal3>=akhir3) { 
        if(!step3){step3=1}
        while(awal3>=akhir3){
            hasil3.push(awal3)
            awal3-=step3;
        }
    }      
    return hasil3.reduce((a, b) => a + b, 0);
}
console.log('console.log(sum(1,10))')
console.log(sum(1,10)) // 55
console.log('console.log(sum(5, 50, 2))')
console.log(sum(5, 50, 2)) // 621
console.log('console.log(sum(15,10))')
console.log(sum(15,10)) // 75
console.log('console.log(sum(20, 10, 2))')
console.log(sum(20, 10, 2)) // 90
console.log('console.log(sum(1))')
console.log(sum(1)) // 1
console.log('console.log(sum())')
console.log(sum()) // 0 


console.log('\n\nSOAL KEEMPAT')
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
console.log('Nomor ID : ' + input[0][0])
console.log('Nama Lengkap : ' + input[0][1])
console.log('TTL : ' + input[0][2]+', '+input[0][3])
console.log('Hobi : ' + input[0][4])
console.log('.')
console.log('Nomor ID : ' + input[1][0])
console.log('Nama Lengkap : ' + input[1][1])
console.log('TTL : ' + input[1][2]+', '+input[1][3])
console.log('Hobi : ' + input[1][4])
console.log('.')
console.log('Nomor ID : ' + input[2][0])
console.log('Nama Lengkap : ' + input[2][1])
console.log('TTL : ' + input[2][2]+', '+input[2][3])
console.log('Hobi : ' + input[2][4])
console.log('.')
console.log('Nomor ID : ' + input[3][0])
console.log('Nama Lengkap : ' + input[3][1])
console.log('TTL : ' + input[3][2]+', '+input[3][3])
console.log('Hobi : ' + input[3][4])
console.log('.')



console.log('\n\nSOAL KELIMA')
function balikKata(pembalikan){
    var finally11 = []
    for(var kebalik = pembalikan.length; kebalik > 0; kebalik--){
        finally11.push(pembalikan[kebalik-1])        
    }
    return finally11.reduce((a, b) => a + b);
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 



console.log('\n\nSOAL KEENAM')
function dataHandling2(input) {
    input.splice(1,1,'Roman Alamsyah Elsharawy')
    input.splice(2,1,'Provinsi Bandar Lampung')
    input.splice(4,1,'pria','SMA Internasional Metro')
    console.log(input)    
    
    var iris1 = input.slice(3,4)
    //console.log(iris1)
    var iris2 = iris1.join('/')
    //console.log(iris2)
    var iris3 = iris2.split('/')
    //console.log(iris3)
    var iris4 = iris3.slice(1,2)
    //console.log(iris4)
    var iris5 = parseInt(iris4)
    //console.log(iris5)
    switch(iris5) {
        case 1 : { console.log('bulan = Januari'); break; }
        case 2 : { console.log('bulan = Februari'); break; }
        case 3 : { console.log('bulan = Maret'); break; }
        case 4 : { console.log('bulan = April'); break; }
        case 5 : { console.log('bulan = Mei'); break; }
        case 6 : { console.log('bulan = Juni'); break; }
        case 7 : { console.log('bulan = Juli'); break; }
        case 8 : { console.log('bulan = Agustus'); break; }
        case 9 : { console.log('bulan = September'); break; }
        case 10 : { console.log('bulan = Oktober'); break; }
        case 11 : { console.log('bulan = November'); break; }
        case 12 : { console.log('bulan = Desember'); break; }
    }  

    var iris33 = iris3
    iris33.sort(function (value1, value2) { return value2 - value1 } ) ;
    console.log(iris33)

    var iris333 = iris2.split('/')
    console.log(iris333.join('-'))

    var iris11 = input.slice(1,2)
    //console.log(iris11)
    var iris12 = iris11.join('/')
    var lengthiris = iris12.length
    //console.log(lengthiris)
    iris13 = iris12.substr(0,lengthiris)
    console.log(iris13)
}



var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
*/

console.log('\n.')
console.log('AKHIRNYA PENDERITAAN INI SELESAI, wkwkwkwk :D')
console.log('\n\n.')