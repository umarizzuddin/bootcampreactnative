import React from 'react'
import { 
    View, 
    Text, 
    StyleSheet, 
    Image, 
    TouchableOpacity,
    ScrollView
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';
//import Icon2 from 'react-native-vector-icons/FontAwesome';

export default class App extends React.Component {
  render() {
      alert('You can scroll this page !')
    return (<ScrollView>
        <View style={styles.mainpage}> 
            <Text style={{fontSize: 40, color:'black', paddingTop: 60}}>About Us</Text>
            <View style={{paddingTop:30}}></View>
            <View style={styles.circle}>
                <View style={styles.logobawah}>
                    <Icon size={40} name="signal"/>
                    <Icon size={48} name="city"/>
                </View>                
                <Text style={styles.logoname}>SanberSans</Text>
            </View>
            <Text style={{fontSize: 20, color:'black', paddingTop: 20}}>SanberSans is one of the biggest</Text>
            <Text style={{fontSize: 20, color:'black'}}>coders comunity around the world.</Text>
            <Text style={{fontSize: 20, color:'black'}}>as the best react-native developer,</Text>
            <Text style={{fontSize: 20, color:'black'}}>we're making changes around the world.</Text>
            <Text style={{fontSize: 20, color:'black', paddingTop: 10}}>make sure your contribution with joining us!</Text>
            <View style={{paddingTop: 90}}/>
            <Text style={{fontSize: 20, color:'black'}}>Our Accounts :</Text>
            <View style={{flexDirection: 'row', paddingTop:25}}>
                <TouchableOpacity style={{marginRight:50, alignItems: 'center', justifyContent: 'center'}}>
                    <Icon size={70} name='gitlab'/>
                    <Text style={{fontSize: 20, color:'black'}}>@SanberSantuy</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{marginLeft:50, alignItems: 'center', justifyContent: 'center'}}>
                    <Icon size={70} name='github'/>
                    <Text style={{fontSize: 20, color:'black'}}>@SanberSans</Text>
                </TouchableOpacity>
            </View>
            <View style={{paddingTop: 90}}/>
            <Text style={{fontSize: 20, color:'black'}}>Contact Us :</Text>
            <View style={{flexDirection: 'column', paddingTop:25}}>
                <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Icon size={50} name='facebook'/>
                    <Text style={{fontSize: 20, color:'black'}}>     Sanber.Santuy</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center', paddingTop:25}}>
                    <Icon size={50} name='instagram'/>
                    <Text style={{fontSize: 20, color:'black'}}>     @SanberSans</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center', paddingTop:25}}>
                    <Icon size={50} name='twitter'/>
                    <Text style={{fontSize: 20, color:'black'}}>     @Sanberindo</Text>
                </TouchableOpacity>
            </View>
            <TouchableOpacity style={{paddingTop: 250, marginLeft:100}}>
            <View style={styles.exitbox}>
                <Icon size={30} name='backward' style={{marginRight:30}}/>
                <Text style={styles.textbox}>Back</Text>
            </View>
            </TouchableOpacity>
            <Text style={{fontSize: 15, color:'black', paddingTop: 70}}>Current version : 20.12.001</Text>
            <View style={{paddingTop: 20}}/>
        </View></ScrollView>
    )
  }
};

const styles = StyleSheet.create({
  mainpage: {flex: 1, alignItems: 'center',backgroundColor:'lightsteelblue'},
  logobawah: {
    paddingTop: 10,
    elevation: 5,
    alignItems: 'flex-end',
    flexDirection: 'row'
  },
  logoname: {fontSize: 25, color: '#3c3c3c'},
  circle: {
    backgroundColor: 'lightgrey',
    borderRadius: 200,
    borderWidth: 1,
    borderColor: 'black',
    width: 200,
    height: 200,
    alignItems: 'center',
    justifyContent: 'center',
  },
  exitbox: {
    width: 200,
    height: 70,
    backgroundColor: 'skyblue',
    borderWidth: 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: 'steelblue',
    borderRadius: 20,
    flexDirection: 'row'
  },
  textbox: {
      fontSize: 25,
      fontWeight:'bold'
  }
})