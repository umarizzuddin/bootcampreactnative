import React from 'react'
import { 
    View, 
    Text, 
    StyleSheet, 
    Image, 
    TouchableOpacity,
    ScrollView
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';
//import Icon2 from 'react-native-vector-icons/FontAwesome';

export default class App extends React.Component {
  render() {
      //alert('You can scroll this page !')
    return (<ScrollView>
        <View style={styles.mainpage}> 
            <View style={styles.logobawah}>
                    <Icon size={35} name="signal"/>
                    <Icon size={42} name="city"/>               
                    <Text style={styles.logoname}>  SanberSans</Text>
            </View> 
            <Text style={{fontSize: 40, color:'black', paddingTop: 100}}>Register</Text>
            <View style={{paddingTop: 30}}/>
            <View style={{justifyContent:'flex-start'}}>
                <Text style={{fontSize: 20}}>Username/Email :</Text>
                <View style={styles.box}/>
            </View>
            <View style={{justifyContent:'flex-start', paddingTop: 30}}>
                <Text style={{fontSize: 20}}>Password :</Text>
                <View style={styles.box}/>
            </View>
            <View style={{justifyContent:'flex-start', paddingTop: 30}}>
                <Text style={{fontSize: 20}}>Confirm Password :</Text>
                <View style={styles.box}/>
            </View>
            
            <TouchableOpacity style={{paddingTop: 80}}>
            <View style={styles.commandbox}>
                <Text style={{fontSize: 23, fontWeight:'bold'}}>SIGN UP</Text>
            </View>
            </TouchableOpacity>
            <TouchableOpacity style={{paddingTop: 30}}>
            <View style={styles.commandbox}>
                <Icon size={30} name='backward' style={{marginRight:30}}/>
                <Text style={{fontSize: 25, fontWeight:'bold'}}>Back</Text>
            </View>
            </TouchableOpacity>
            <Text style={{fontSize: 15, color:'black', paddingTop: 70}}>Current version : 20.12.001</Text>
            <View style={{paddingTop: 20}}/>
        </View></ScrollView>
    )
  }
};

const styles = StyleSheet.create({
  mainpage: {flex: 1, alignItems: 'center',backgroundColor:'lightsteelblue'},
  logobawah: {
    paddingTop: 80,
    elevation: 5,
    alignItems: 'flex-end',
    flexDirection: 'row'
  },
  logoname: {fontSize: 30, color: '#3c3c3c'},
  box: {
    width: 350,
    height: 40,
    backgroundColor: 'lightgrey',
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#3c3c3c',
  },
  commandbox: {
    width: 200,
    height: 60,
    backgroundColor: 'skyblue',
    borderWidth: 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: 'steelblue',
    flexDirection: 'row'
  }
})