import React from 'react'
import { 
    View, 
    Text, 
    StyleSheet, 
    Image, 
    TouchableOpacity,
    ScrollView
} from 'react-native'
//import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon2 from 'react-native-vector-icons/FontAwesome5';

export default class App extends React.Component {
  render() {
      //alert('Welcome to SanberSans App !')
    return (<ScrollView>
        <View style={styles.mainpage}> 
            <View style={styles.logoatas}>
            <Icon2 size={50} name="signal"/>
            <Icon2 size={60} name="city"/>
            </View>                
            <Text style={styles.logoname}>SanberSans</Text>
            <Text style={{fontSize: 40, color:'black', paddingTop: 50}}>Welcome to</Text>
            <Text style={{fontSize: 40, color:'black'}}>SanberSans Project!</Text>
            <Text style={{fontSize: 30, color:'black', paddingTop: 20}}>We're finding for</Text>
            <Text style={{fontSize: 30, color:'black'}}>multi-talent coders</Text>
            <Text style={{fontSize: 30, color:'black'}}>to walk together.</Text>
            <TouchableOpacity style={{paddingTop: 85}}>
            <View style={styles.box}>
                <Text style={styles.textbox}>Register</Text>
            </View>
            </TouchableOpacity>
            <TouchableOpacity style={{paddingTop: 50}}>
            <View style={styles.box}>
                <Text style={styles.textbox}>Login</Text>
            </View>
            </TouchableOpacity>
            <TouchableOpacity style={{paddingTop: 50}}>
            <View style={styles.box}>
                <Text style={styles.textbox}>About Us</Text>
            </View>
            </TouchableOpacity>
            <TouchableOpacity style={{paddingTop: 50}}>
            <View style={styles.exitbox}>
                <Text style={styles.textbox}>Quit</Text>
            </View>
            </TouchableOpacity>
            <View style={styles.logobawah}>
                <Icon2 size={30} name="signal"/>
                <Icon2 size={35} name="city"/>
            </View>                
            <Text style={styles.logoname}>SanberSans</Text>
            <Text style={{fontSize: 15, color:'black', paddingTop: 70}}>Current version : 20.12.001</Text>
            <View style={{paddingTop: 20}}/>
        </View></ScrollView>
    )
  }
};

const styles = StyleSheet.create({
  mainpage: {flex: 1, alignItems: 'center',backgroundColor:'lightsteelblue'},
  logoatas: {
    paddingTop: 70,
    elevation: 5,
    alignItems: 'flex-end',
    flexDirection: 'row'
  },
  logobawah: {
    paddingTop: 80,
    elevation: 5,
    alignItems: 'flex-end',
    flexDirection: 'row'
  },
  logoname: {fontSize: 25, color: '#3c3c3c'},
  box: {
    width: 300,
    height: 60,
    backgroundColor: 'skyblue',
    borderWidth: 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: 'steelblue',
    borderRadius: 20,
  },
  exitbox: {
    width: 300,
    height: 60,
    backgroundColor: 'grey',
    borderWidth: 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: 'steelblue',
    borderRadius: 20,
  },
  textbox: {
      fontSize: 25,
      fontWeight:'bold'
  }
})