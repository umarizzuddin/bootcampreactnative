import React from 'react';
import { View, Text, ScrollView, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

export default class Note extends React.Component {
 render(){
   return(
     <View key={this.props.keyval} style={styles.note}><TouchableOpacity onPress={this.props.openFile}>
       <Text style={styles.noteText}>{this.props.val.note}</Text>
       <Text style={styles.noteText}>{this.props.val.date}</Text></TouchableOpacity>

       <TouchableOpacity onPress={this.props.deleteMethod} style={styles.noteDelete}>
         <Icon name="delete" style={styles.noteDeleteText} size={22}/>
       </TouchableOpacity>
     </View>
   )
 }
};

const styles = StyleSheet.create({
  note: {
    position: 'relative',
    padding: 20,
    paddingRight: 100,
    borderBottomWidth: 4,
    borderBottomColor: '#ededed',
    backgroundColor: 'lightsteelblue',
  },
  noteText: {
    paddingLeft: 20,
    borderLeftWidth: 10,
    borderLeftColor: '#2980b9',
  },
  noteDelete: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2980b9',
    padding: 10,
    top: 10,
    bottom: 10,
    right: 10
  },
  noteDeleteText: {
    color: 'white',
  }
});

