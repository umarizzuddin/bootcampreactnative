import React from 'react'
import { 
    View, 
    Text, 
    StyleSheet, 
    Image, 
    TouchableOpacity,
    ScrollView
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './videozz'
import Data from './data.json';

export default class App extends React.Component {
  render() {
      alert('Make sure your internet connection available')
    return (
        <View style={styles.mainpage}>
            <View style={styles.topbar}>
                <Image source={require('./logo.png')} style={{width:98, height:22}}/>
                <View style={styles.topbuttons}>
                    <TouchableOpacity>
                        <Icon size={25} style={styles.topitems} name="search"/>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Icon size={25} style={styles.topitems} name="account-circle"/>
                    </TouchableOpacity>
                </View>
            </View>
            
            <View style={styles.pagebody}>
                <VideoItem/>
            </View>
            
            <View style={styles.downbar}>
                    <TouchableOpacity style={styles.menu}>
                        <Icon size={25} name="home"/>
                        <Text style={styles.menutitle}>Home</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.menu}>
                        <Icon size={25} name="local-fire-department"/>
                        <Text style={styles.menutitle}>Trendings</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.menu}>
                        <Icon size={25} name="subscriptions"/>
                        <Text style={styles.menutitle}>subscriptions</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.menu}>
                        <Icon size={25} name="folder"/>
                        <Text style={styles.menutitle}>Library</Text>
                    </TouchableOpacity>
            </View>
        </View>
    )
  }
};

const styles = StyleSheet.create({
  mainpage: {flex: 1},
  topbar: {
    height: 70,
    paddingHorizontal: 25,
    elevation: 5,
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  topbuttons: {flexDirection:'row'},
  topitems: {marginLeft:25},
  pagebody : {flex: 1},
  downbar: {
      backgroundColor: 'grey',
      height: 60,
      borderTopWidth: 0.5,
      borderColor: 'red',
      flexDirection: 'row',
      justifyContent: 'space-around'
  },
  menutitle: {fontSize: 11, color: '#3c3c3c', paddingTop: 4},
  menu: {alignItems: 'center', justifyContent: 'center'}
})